**********************************
*Job Scheduler System Version 1.0*
*@Tianhao Chen                   *
*Date: 2018/2/26                 *
**********************************


Please run "Job_Schedulers_Cubist_Tianhao_Chen.exe"

*********************
System requirement: 
If the "Job_Schedulers_Cubist_Tianhao_Chen.exe" cannot run. Please use the file in .zip file to biuld a project in Visual Studio. 
1. Boost 1.63.0 or more recent version is required. Since I use boost::posix and boos
2. Please run the program in Visual Studio C++ released mode x86 (some boost libraries do not support x64)

*********************
Program introduction:

This program aim to generate future and today schedules.
Allow user to access and re-run historical task.
As a prototype, the program will automatically generate 2 historical schedules, 1 today schedule including 50 jobs, 
and 1 future schedule include 5 jobs. These sample data only use for testing the program.

The .zip include 4 .h files and 1 .cpp file.

*job.h
	- Define Job class, which store date and description of the job.
	- User can create new job, view/get job attributes by its functions.
	- User can run job and check dependency of a job.
	- User can force to run a job, ignoring the dependency.

*schedule.h
	- Define Schedule class, which store raw pointers that point to each job.
	- User can add new job by time manner.
	- User can run one or run all job with or without dependecy.

*historicalLogSystem.h
	- Define HistoricalLogSystem, which contains a map key on boost:gregorian::date,value on raw pointer of schedule.
	- User can check how many schedule in the system by showing user the date.
	- User can run a job or all job of a certain date by providing the date.

*userInterface.h
	- Define a interface which control and check I/O in a while loop.

*main.cpp
	- The main function that auto generate jobs and schedules, and run the system afterwards.

********************
Program Instruction:
Once the program start, user will be asked to enter today's date by string of required format. 
The program will automatically generate jobs and schedules, and run today's jobs.

User can use the following command to control the program:
	1. Enter 'view schedule' to view historical schedule.
	2. Enter 'view tomorrow' to view tomorrow's schedule.
	3. Enter 'view today' to view today's schedule.
	4. Enter 'run today' to run today's job.
	5. Enter 'run today all' to run all today's job.
	6. Enter 'run historical job' to run previous job by date and time.
	7. Enter 'run a day historical jobs' to run all jobs in a previous date.
	8. Enter 'add job' to add an unfinished job by date and time.
	9. Enter 'help' to view this instruction.
	10.Enter 'EXIT' to exit the system.

Suggestions:
	1. You can always enter 'help' to view instruction.
	2. It would be better to view schedule to a certain day to see what jobs are on the list.
	3. The dependency of a job is provided by user by entering an exist job datetime. 
	   So, better see what job exist.
	4. The interface is user friendly enough to make user understand what he need to do in 
	   the next step. Try to follow the instruction showed on the interface.

***CAUTION***
For the duration input, I failed to add a input value check. 
The program may fail if user does not enter a valid number.
Please try to follow the instructions showing in the programs interface to avoid program crash.

******************
PS:
1. For simplicity, the job will use std::this_thread::sleep() to simulate doing some jobs.
2. The program tried to implement multi-threading in order to give quick respond to user,
while doing other job. This part of job can still be modified and imporved.
Now, the prototype only multi-thread the running of today's jobs, but not on other jobs.
3. Some functionalities that contained by the class are still not available in the interface.
For example, delete job and add schedule to history. More I/O controls are stilling under developed.
4. The program after improvement should allow user to input/output schedule to/from .csv/.txt.

******************
Thank you for your time and patience for this program.
Also, any suggestion is graciously welcomed.
