/**
* historicalLogSystem.h
* Definition of historicalLogSystem class, which store historical job schedules.
* in case user want to view of run them.
*
* @Tianhao Chen
* 2/24/2018
*/

#ifndef HISTORICALLOGSYSTEM_H
#define HISTORICALLOGSYSTEM_H

#include <map>

#include "schedule.h"
#include "job.h"

class historicalLogSystem
{
private:
	std::map<Date, Schedule*> _historical_schedules;

public:
	// Ctor and dtor
	historicalLogSystem();
	~historicalLogSystem();

	// Access functions
	void viewHistoricalDate() const;
	void viewHistoricalSchedule(std::string date) const;	// View a certain day's schedule.
	Schedule getHistotoricalSchedule(std::string date);		// Get acertain day's schedule.

	// Modify functions
	void addSchedule(Schedule& schedule, bool is_override = 0);					// Add schedule to historical log.
};

// Ctor and dtor
historicalLogSystem::historicalLogSystem()
{
}

historicalLogSystem::~historicalLogSystem() 
{
}

void historicalLogSystem::viewHistoricalDate() const
{ // View all historical dates in the log.

	for (auto it = _historical_schedules.begin(); it != _historical_schedules.end(); ++it)
	{
		std::cout << it->first << std::endl;
	}
}

void historicalLogSystem::viewHistoricalSchedule(std::string date) const
{ // View certain day's schedule.

	_historical_schedules.at(boost::gregorian::from_simple_string(date))->viewSchedule();
}

Schedule historicalLogSystem::getHistotoricalSchedule(std::string date)
{ // Get certain day's schedule.

	return *(_historical_schedules[boost::gregorian::from_simple_string(date)]);
}

void historicalLogSystem::addSchedule(Schedule& schedule, bool is_override)
{
	if (_historical_schedules.count(schedule.getDate()) == 0)
	{ // If there is no schedule there, insert one.

		_historical_schedules[schedule.getDate()] = &schedule;
	}
	else if(is_override == 0)
	{ // If there is schedule there and user don't want to override. Do nothing.
	
		std::cout << "There already has a schedule at this date." << std::endl;
	}
	else
	{ // If there is schedule there and user want to override.

		std::cout << "Override the schedule of: " << schedule.getDate() << std::endl;
		_historical_schedules[schedule.getDate()] = &schedule;
	}
}

#endif // !HISTORICALLOGSYSTEM_H
