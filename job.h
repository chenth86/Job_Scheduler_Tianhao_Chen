/**
* job.h
* Definition of job base class
*
* @Tianhao Chen
* 2/24/2018
*/

#ifndef JOB_H
#define JOB_H

#include <string>
#include <chrono>
#include <thread>
#include <mutex>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

typedef boost::gregorian::date Date;
typedef boost::posix_time::ptime Datetime;


class Job
{ // Job base class.

private:
	Date _date;						// Date of the job.
	Datetime _datetime;				// Date and time of the job.
	std::string _job_description;	// The content of the job.
	Job* _dependency = nullptr; // Initialize the job dependency as nullptr.
	bool _job_status;				// Indicator of the accomplishment of the job.
									// 1 == done, 0 == not done.
	int _duration;

public:
	
	// Ctor and dtor
	Job() = default;
	Job(Datetime datetime, std::string job_description, int duration = 5, bool job_status = 0);
	Job(Datetime datetime, std::string job_description, Job& dependency, int duration = 5, bool job_status =0);
	Job(const Job& job);

	virtual ~Job();

	// Access functions
	void viewDate() const;		// View date.
	void viewDatetime() const;	// View date and time.
	void viewJob() const;		// View job description.
	void viewStatus() const;	// View whether job is done or not.


	// Access functions with return value.
	Date getDate() const;			// Get date.
	Datetime getDatetime() const;	// Get datetime.
	int getDuration() const;		// Get duration.
	std::string getJob() const;		// Get job description.
	bool getStatus() const;			// Get job status.
	bool checkDependency() const;	// Check the dependency, 1 for has, 0 for none.
	Job getDependency() const;		// Get the dependency of the job.

	// Run the job
	void run_job(bool force_to_run = 0);
	void run_job_thread(bool force_to_run = 0);
};

Job::Job(Datetime datetime, std::string job_description, int duration, bool job_status)
	: _datetime(datetime), _job_description(job_description), _duration(duration), _job_status(job_status)
{ // Ctor.

	_date = datetime.date();
}

Job::Job(Datetime datetime, std::string job_description, Job& dependency, int duration, bool job_status) 
	: _datetime(datetime), _job_description(job_description), _duration(duration), _job_status(job_status)
{ //Ctor with dependency.

	_dependency = &dependency;
	_date = datetime.date();
}

Job::Job(const Job& job): _date(job.getDate()), _datetime(job.getDatetime())
 , _job_description(job.getJob()), _duration(job.getDuration()), _job_status(job.getStatus())
{
	if (job.checkDependency() == 1)
		_dependency = &job.getDependency();
	else
		_dependency = nullptr;
}


Job::~Job()
{ // Dtor.

}

void Job::viewDate() const
{ // Print the date.

	std::cout << _date << std::endl;
}

void Job::viewDatetime() const
{ // Print the date and time.

	std::cout << _datetime;
}

void Job::viewJob() const
{ // Print job description.
	
	std::cout << _datetime << ". " << _job_description << ". ";
	if (_dependency == nullptr) 
		std::cout << "No dependency required. ";
	else
	{
		std::cout << "Depend on job at: ";
		_dependency->viewDatetime(); 
		std::cout << ". ";
	}
	std::cout << "Job duration: " << getDuration() << " minutes. ";
	viewStatus();
}

void Job::viewStatus() const
{ // Print job status.

	(_job_status == 1) ? (std::cout << getJob() << " done." << std::endl) : (std::cout << getJob() << " not done." << std::endl);
}

Date Job::getDate() const
{ // Get job date.

	return _date;
}

Datetime Job::getDatetime() const
{ // Get job date and time.

	return _datetime;
}

int Job::getDuration() const
{ // Get job duration.

	return _duration;
}

std::string Job::getJob() const
{ // Get job description.

	return _job_description;
}

bool Job::getStatus() const
{ // Get job status.

	return _job_status;
}

bool Job::checkDependency() const
{ // Check the dependency of the job.

	return (_dependency == nullptr) ? 0 : 1;
}

Job Job::getDependency() const
{ // Get the dependency of the job.

	if (_dependency == nullptr)
	{ // Handle the case when no job dependency required. Return default value.

		std::cout << "This is job has no dependency" << std::endl;
		return Job(boost::posix_time::time_from_string("2000-01-01 00:00:00"),"");
	}

	return *_dependency;
}

void Job::run_job(bool force_to_run)
{ // Run the job. Simulate job running by using sleep for the certain duration. 
  // Here for practical reason, we let sleep 1 second represent doing job for 1 minute.

	if (force_to_run == 1)
	{ // run the job nontheless.
 
		std::cout << "User forces to run: '" 
			<< _job_description << "'";
		_job_status = 1;						// Change job status to done. 
		std::this_thread::sleep_for(std::chrono::seconds(_duration));
		std::cout << "Job done." << std::endl;
		return;
	}

	if (_dependency != nullptr)
	{ // Run when dependency is required.

		if (_dependency->getStatus() == 1)
		{ // Check the whether the dependency is completed.

			std::cout << "Dependency requirement met. Run current job: '" 
				<< _job_description << "'";
			std::this_thread::sleep_for(std::chrono::seconds(_duration));
			std::cout << "Job done." << std::endl;

			_job_status = 1;					// Change job status to done.
		}

		else
		{ // If dependency not comspleted.

			std::cout << "Cannot run current job. The previous job: '" 
				<< _dependency->getJob() << "' needs to be run first." << std::endl;
		}
	}
	else
	{ // Run when no dependency required.

		// std::cout << "Run current job: '" << _job_description << "'" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(_duration));
		// std::cout << "Job done." << std::endl;

		_job_status = 1;						// Change job status to done.
	}
}

void Job::run_job_thread(bool force_to_run)
{
	std::thread t(&Job::run_job, this, force_to_run);
	t.join();
}



#endif// !JOB_H
