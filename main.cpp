/**
* main.cpp
* Define the job scheduler system.
* The main program.
*
* @Tianhao Chen
* 2/24/2018
*/

#include <iostream>

#include "job.h"
#include "schedule.h"
#include "historicalLogSystem.h"
#include "userInterface.h"


std::map<Datetime, Job> all_jobs;

int main()
{
	// Initialize today's date
	unsigned int num = 5;
	std::string input;
	std::cout << "Please enter today's date in the format: YYYY-MM-DD. To generate schedules." << std::endl;
	std::getline(std::cin, input);
	while (checkDate(input) == 0)
	{ // Check validity of date input.

		std::getline(std::cin, input);
	}
	Date d = boost::gregorian::from_simple_string(input);

	// Initialize sample historical schedules.
	historicalLogSystem _historicalLog;
	Schedule schedule1 = generate_schedule(d - boost::gregorian::date_duration(2), num, all_jobs, 2, 1);
	_historicalLog.addSchedule(schedule1);
	Schedule schedule2 = generate_schedule(d - boost::gregorian::date_duration(1), num, all_jobs, 2, 1);
	_historicalLog.addSchedule(schedule2);
	Schedule schedule_today = generate_schedule(d, num * 10, all_jobs, 2, 0);
	_historicalLog.addSchedule(schedule_today);
	Schedule schedule_tomorrow = generate_schedule(d + boost::gregorian::date_duration(1), num, all_jobs, 2, 0);
	_historicalLog.addSchedule(schedule_tomorrow);
	std::cout << "Sample schedule generated." << std::endl << std::endl;
	
	// Print instructions.
	Instruction();

	// Run todays job.
	std::cout << "Run all today's job" << std::endl;
	std::thread t2(&Schedule::run_All_Job , schedule_today, 0);
	
	
	// Main loop for getting users input.
	std::thread t1(Whileloop, _historicalLog, input, d, all_jobs);
	
	if (t2.joinable()) t2.join();
	t1.join();
	// t2.join();


	return 0;
}



