/**
* schedule.h
* Definition of schedule class, which store upcoming job schedule.
* Functionality:
* Allow view schedule, add job, delete job, get (search) job.
* Allow run all jobs in the schedule.
*
* @Tianhao Chen
* 2/24/2018
*/

#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <list>

#include "job.h"

class Schedule
{
private:
	Date _date;								// Schedule date. In case user run previous date jobs.
	std::list<Job*> _job_list;				// List of pointers to Upcoming jobs.

public:
	// Ctor and dtor
	Schedule(Date date);
	Schedule(Schedule& schedule);
	~Schedule();

	// Access functions
	void viewSchedule() const;				// View all jobs in the list.
	void viewJob(std::string time) const;	// View the job by time.
	Date getDate() const;					// Get the schedule date.
	std::list<Job*> getSchedule() const;		// Get the list of jobs.
	Job& getJob(std::string time) const;		// Get the job by time.

	// Modify functions
	void addJob(Job& job);					// Add a new job according to time.
	void deleteJob(std::string time);		// Delete a job according to time.

	void run_Job(std::string time, bool force_to_run = 0);		// Run one job by time.
	void run_All_Job(bool force_to_run = 0);					// Run all jobs in the list.
	void run_All_Job_thread(bool force_to_run = 0);
};


Schedule::Schedule(Date date): _date(date)
{ // Ctor. Initialize date.
}

Schedule::Schedule(Schedule& schedule): _date(schedule.getDate()), _job_list(schedule.getSchedule())
{ // Copy constructor
}

Schedule::~Schedule()
{
}

void Schedule::viewSchedule() const
{ // View all jobs on schedule.

	if (_job_list.empty() == 1)
	{ // Handle the case when job list is empty.

		std::cout << "There is no job in the schedule." << std::endl;
	}

	// When the list has job.
	for (auto job : _job_list)
	{
		job->viewJob();
	}
}

void Schedule::viewJob(std::string time) const					
{ // View the job by time.

	if (_job_list.empty() == 1)
	{ // Handle the case when no job in the list.

		std::cout << "There is no job in the schedule." << std::endl;
		return;
	}

	// When there are jobs on the list.
	std::string date_string = boost::gregorian::to_iso_extended_string(_date);
	auto date_time = boost::posix_time::time_from_string(date_string + " " + time);
	for (auto job : _job_list)
	{
		if (job->getDatetime() == date_time)
		{
			job->viewJob();
			return;
		}
	}

	// Handle the case when user's input time has no job to be done. Return default value.
	std::cout << "There is no job schedule at your input time point." << std::endl;
}
Date Schedule::getDate() const
{ // Get the date of the schedule.

	return _date;
}

std::list<Job*> Schedule::getSchedule() const
{ // Get the job list.

	return _job_list;
}

Job& Schedule::getJob(std::string time) const
{// Get the job by time.

	if (_job_list.empty() == 1)
	{ // Handle the case when no job in the list.

		std::cout << "There is no job in the schedule." << std::endl;
		return Job(boost::posix_time::time_from_string("2000-01-01 00:00:00"), "");
	}

	// When there are jobs on the list.
	std::string date_string = boost::gregorian::to_iso_extended_string(_date);
	auto date_time = boost::posix_time::time_from_string(date_string + " " + time);
	for (auto job : _job_list)
	{
		if (job->getDatetime() == date_time)
		{
			return *job; 
		}
	}
	
	// Handle the case when user's input time has no job to be done. Return default value.
	std::cout << "There is no job schedule at your input time point." << std::endl;
	return Job(boost::posix_time::time_from_string("2000-01-01 00:00:00"), "");
}

void Schedule::addJob(Job& job)
{ // Add new job to the schedule according to time.

	auto date_time = job.getDatetime();

	if (_job_list.begin() == _job_list.end())
	{ // If list is empty, insert at beginning.

		_job_list.push_back(&job);
		return;
	}

	for (auto it = _job_list.begin(); it != _job_list.end(); ++it)
	{ // Find the first point that new job schedule is before that time. Insert new job before it.

		if (date_time < (*it)->getDatetime())
		{ 
			_job_list.insert(it, &job);
			return;
		}
	}

	// If insertion is the last job, insert at back.
	_job_list.push_back(&job);
	
}

void Schedule::deleteJob(std::string time)
{ // Delete the job by the certain time.

	if (_job_list.empty() == 1)
	{ // Handle the case when no job in the list.

		std::cout << "There is no job in the schedule." << std::endl;
		return;
	}

	// Transform time from input type std::string to Datetime type.
	std::string date_string = boost::gregorian::to_iso_extended_string(_date);
	auto date_time = boost::posix_time::time_from_string(date_string + " " + time);

	for (auto it = _job_list.begin(); it != _job_list.end(); ++it)
	{ // Find the job with certain date time. Delete it from the schedule.

		if ((*it)->getDatetime() == date_time)
		{
			_job_list.erase(it);
			return;
		}
	}

	// Handle the case when user's input time has no job to be done.
	std::cout << "There is no job schedule at your input time point." << std::endl;
}

void Schedule::run_Job(std::string time, bool force_to_run)	
{ // Run one job by time.

	if (_job_list.empty() == 1)
	{ // Handle the case when no job in the list.

		std::cout << "There is no job in the schedule." << std::endl;
		return;
	}

	// Transform time from input type std::string to Datetime type.
	std::string date_string = boost::gregorian::to_iso_extended_string(_date);
	auto date_time = boost::posix_time::time_from_string(date_string + " " + time);

	for (auto it = _job_list.begin(); it != _job_list.end(); ++it)
	{ // Find the job with certain date time. Delete it from the schedule.

		if ((*it)->getDatetime() == date_time)
		{
			(*it)->run_job(force_to_run);
			return;
		}
	}

	// Handle the case when user's input time has no job to be done.
	std::cout << "There is no job schedule at your input time point." << std::endl;

}

void Schedule::run_All_Job(bool force_to_run)
{ // Run all the jobs in the list.

	// Handle no job in list case.
	if (_job_list.empty() == 1)
	{
		std::cout << "Cannot run. There is no job in the schedule." << std::endl;
		return;
	}

	// If there are jobs in the list.
	for (auto it = _job_list.begin(); it != _job_list.end(); ++it)
	{
		(*it)->run_job(force_to_run);
	}
	std::cout << "All " + boost::gregorian::to_iso_extended_string(_date) + " jobs done." << std::endl;
}

void Schedule::run_All_Job_thread(bool force_to_run)
{
	// auto func = std::bind(&Schedule::run_All_Job, this);
	std::thread t(&Schedule::run_All_Job, this, force_to_run);
	t.detach();
}

#endif // !SCHEDULE_H







