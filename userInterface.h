/**
* userInterface.h
* Define the user interface:
* 
* I/O control.
* Instruction.
* Check input validity.
* 
* @Tianhao Chen
* 2/25/2018
*/

#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <string>
#include <iostream>
#include <vector>
#include <thread>
#include <functional>

#include "job.h"
#include "schedule.h"
#include "historicalLogSystem.h"



void Instruction()
{ // Operation instruction.

	std::cout << "-----INSTRUCTIONS-----" << std::endl;
	std::cout << "Enter 'view schedule' to view historical schedule." << std::endl;
	std::cout << "Enter 'view tomorrow' to view tomorrow's schedule." << std::endl;
	std::cout << "Enter 'view today' to view today's schedule." << std::endl;
	std::cout << "Enter 'run today' to run today's job." << std::endl;
	std::cout << "Enter 'run today all' to run all today's job." << std::endl;
	std::cout << "Enter 'run historical job' to run previous job by date and time." << std::endl;
	std::cout << "Enter 'run a day historical jobs' to run all jobs in a previous date." << std::endl;
	std::cout << "Enter 'add job' to add an unfinished job by date and time." << std::endl;
	std::cout << "Enter 'help' to view this instruction." << std::endl;
	std::cout << "Enter 'EXIT' to exit the system." << std::endl;
	std::cout << "----------------------" << std::endl;
}

bool checkDate(std::string date)
{ // Check user input date validity. Good input should be: "YYYY-MM-DD".
  // Function still need to modify to check each case of each month respectively.

	if (date.size() != 10)
		return 0;
	if (date[4] != '-' || date[7] != '-')
		return 0;
	if (std::stoi(date.substr(0, 4)) <1950 || std::stoi(date.substr(0, 4)) > 2050
		|| std::stoi(date.substr(5, 2)) < 1 || std::stoi(date.substr(5, 2)) > 12
		|| std::stoi(date.substr(8, 2)) > 31)
	{
		return 0;
	}

	return 1;
}

bool checkTime(std::string time)
{ // Check user input time validity. Good input should be: "HH:MM:SS". 

	if (time.size() != 8)
		return 0;
	if (time[2] != ':' || time[5] != ':')
		return 0;
	if (std::stoi(time.substr(0, 2)) <0 || std::stoi(time.substr(0, 4)) > 23
		|| std::stoi(time.substr(3, 2)) < 0 || std::stoi(time.substr(3, 2)) > 59
		|| std::stoi(time.substr(6, 2)) < 0 || std::stoi(time.substr(6, 2)) > 59)
	{
		return 0;
	}
	return 1;
}

bool checkDateTime(std::string datetime)
{ // Check user input date time. Good input should be: "YYYY-MM-DD HH:MM:SS".

	if (datetime.size() != 19)
		return 0;
	if (checkDate(datetime.substr(0, 10)) == 0 || checkTime(datetime.substr(11, 8)) == 0 || datetime[10] != ' ')
		return 0;
	return 1;
}

std::vector<Job> generate_jobs(Date d, unsigned int num, std::map<Datetime, Job>& all_jobs, int duration = 5, bool status = 0)
{ // Generate nums jobs in a vector.

	std::vector<Job> job_list;
	// Date d = boost::gregorian::from_simple_string(date);

	std::string string_dt = boost::gregorian::to_iso_extended_string(d) + " " + "07:00:00";
	Datetime dt = boost::posix_time::time_from_string(string_dt);

	for (unsigned int i = 1; i <= num; ++i)
	{
		auto s = "Job_" + std::to_string(i) + "_" + boost::gregorian::to_iso_extended_string(d);
		Job job(dt, s, duration, status);
		job_list.push_back(job);
		all_jobs[dt] = job;
		dt += boost::posix_time::minutes(10);
	}

	return job_list;
}

Schedule generate_schedule(Date d, unsigned int num, std::map<Datetime, Job>& all_jobs, int duration = 5, bool status = 0)
{ // Generate a single day job list.

  // Date d = boost::gregorian::from_simple_string(date);
	Schedule new_schedule(d);

	auto jobs = generate_jobs(d, num, all_jobs, duration, status);
	for (auto job : jobs)
	{
		new_schedule.addJob(all_jobs[job.getDatetime()]);
	}

	return new_schedule;
}

void Whileloop(historicalLogSystem& _historicalLog, std::string input, Date d, std::map<Datetime, Job>& all_jobs)
{ // Main loop to get input from user.
  // string input for user input; string d for today's date; _historicalLog for historicalLogSystem.

	while (true)
	{ // Keep user interface open until user input "EXIT".

		std::getline(std::cin, input);
		while (input.empty())
		{ // Check not to get empty line.

			std::getline(std::cin, input);
		}
		if (input == "help")
		{ // Show instruction.

			Instruction();
			std::cout << "Please give another command." << std::endl;
			continue;
		}
		else if (input == "EXIT")
		{ // Exit the program.

			std::cout << "Exit the system." << std::endl;
			break;
		}
		else if (input == "view schedule")
		{ // View historical schedule.

			_historicalLog.viewHistoricalDate();
			std::cout << "Please enter a date in the following format: YYYY-MM-DD." << std::endl;
			std::getline(std::cin, input);
			while (checkDate(input) == 0)
			{ // Check validity of date input.

				std::cout << "Invalid format. Please enter again: YYYY-MM-DD." << std::endl;
				std::getline(std::cin, input);
			}
			// View historical schedule.
			_historicalLog.viewHistoricalSchedule(input);
			std::cout << "----------------------" << std::endl;
			std::cout << "Please give another command." << std::endl;
			continue;
		}
		else if (input == "view tomorrow")
		{ // View tomorrow's schedule.

			_historicalLog.viewHistoricalSchedule(boost::gregorian::to_iso_extended_string(d + boost::gregorian::date_duration(1)));
			std::cout << "----------------------" << std::endl;
			std::cout << "Please give another command." << std::endl;
			continue;
		}
		else if (input == "view today")
		{ // View today's schedule.

			_historicalLog.viewHistoricalSchedule(boost::gregorian::to_iso_extended_string(d));
			std::cout << "----------------------" << std::endl;
			std::cout << "Please give another command." << std::endl;
			continue;
		}
		else if (input == "run today all")
		{ // Run all jobs of today.

			std::cout << "Force to run?[Y/N]" << std::endl;
			std::getline(std::cin, input);
			while (input != "N" && input != "Y")
			{ // Check validity of input.

				std::cout << "Please enter 'Y' or 'N'" << std::endl;
				std::getline(std::cin, input);
			}
			if (input == "N")
			{
				_historicalLog.getHistotoricalSchedule(boost::gregorian::to_iso_extended_string(d)).run_All_Job();
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
			else
			{
				_historicalLog.getHistotoricalSchedule(boost::gregorian::to_iso_extended_string(d)).run_All_Job(1);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}	
		}
		else if (input == "run today")
		{ // Run one job of today.

			std::cout << "Enter the job time in format: HH:MM:SS" << std::endl;
			std::getline(std::cin, input);
			while (checkTime(input) == 0)
			{ // Check validity of time input.

				std::cout << "Invalid format. Please enter again: HH:MM:SS." << std::endl;
				std::getline(std::cin, input);
			}
			auto time = input;

			// Run the job. Ask user whether force to run.
			std::cout << "Force to run?[Y/N]" << std::endl;
			std::getline(std::cin, input);
			while (input != "N" && input != "Y")
			{ // Check validity of input.

				std::cout << "Please enter 'Y' or 'N'" << std::endl;
				std::getline(std::cin, input);
			}
			if (input == "N")
			{
				_historicalLog.getHistotoricalSchedule(boost::gregorian::to_iso_extended_string(d)).run_Job(time);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
			else
			{
				_historicalLog.getHistotoricalSchedule(boost::gregorian::to_iso_extended_string(d)).run_Job(time, 1);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
		}
		else if (input == "run historical job")
		{ // Run one historical job of a day.

			std::cout << "Enter the job date in format: YYYY-MM-DD" << std::endl;
			std::getline(std::cin, input);
			while (checkDate(input) == 0)
			{ // Check validity of date input.

				std::cout << "Invalid format. Please enter again: YYYY-MM-DD." << std::endl;
				std::getline(std::cin, input);
			}
			auto date = input;

			std::cout << "Enter the job time in format: HH:MM:SS" << std::endl;
			std::getline(std::cin, input);
			while (checkTime(input) == 0)
			{ // Check validity of time input.

				std::cout << "Invalid format. Please enter again: HH:MM:SS." << std::endl;
				std::getline(std::cin, input);
			}
			auto time = input;

			// Run the job. Ask user whether force to run.
			std::cout << "Force to run?[Y/N]" << std::endl;
			std::getline(std::cin, input);
			while (input != "N" && input != "Y")
			{ // Check validity of input.

				std::cout << "Please enter 'Y' or 'N'" << std::endl;
				std::getline(std::cin, input);
			}
			if (input == "N")
			{
				_historicalLog.getHistotoricalSchedule(date).run_Job(time);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
			else
			{
				_historicalLog.getHistotoricalSchedule(date).run_Job(time, 1);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
		}
		else if (input == "run a day historical jobs")
		{ // Run all historical jobs of a day.

			std::cout << "Enter the job date in format: YYYY-MM-DD" << std::endl;
			std::getline(std::cin, input);
			while (checkDate(input) == 0)
			{ // Check validity of date input.

				std::cout << "Invalid format. Please enter again: YYYY-MM-DD." << std::endl;
				std::getline(std::cin, input);
			}
			auto date = input;

			// Run all jobs in that day. Ask user whether force to run.
			std::cout << "Force to run?[Y/N]" << std::endl;
			std::getline(std::cin, input);
			while (input != "N" && input != "Y")
			{ // Check validity of input.

				std::cout << "Please enter 'Y' or 'N'" << std::endl;
				std::getline(std::cin, input);
			}
			if (input == "N")
			{
				_historicalLog.getHistotoricalSchedule(date).run_All_Job();
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
			else
			{
				_historicalLog.getHistotoricalSchedule(date).run_All_Job(1);
				std::cout << "----------------------" << std::endl;
				std::cout << "Please give another command." << std::endl;
				continue;
			}
		}
		else if (input == "add job")
		{ // Add a new job

			std::cout << "Enter the job date in format: YYYY-MM-DD" << std::endl;
			std::getline(std::cin, input);
			while (checkDate(input) == 0)
			{ // Check validity of date input.

				std::cout << "Invalid format. Please enter again: YYYY-MM-DD." << std::endl;
				std::getline(std::cin, input);
			}
			auto date = input;

			std::cout << "Enter the job time in format: HH:MM:SS" << std::endl;
			std::getline(std::cin, input);
			while (checkTime(input) == 0)
			{ // Check validity of time input.

				std::cout << "Invalid format. Please enter again: HH:MM:SS." << std::endl;
				std::getline(std::cin, input);
			}
			auto datetime = date + " " + input;

			std::cout << "Enter the job description" << std::endl;
			std::getline(std::cin, input);
			while (input.empty())
			{ // Check not to get empty line.

				std::getline(std::cin, input);
			}
			auto job_description = input;

			// Check job dependency requirement.
			std::cout << "Dependency required?[Y/N]" << std::endl;
			std::getline(std::cin, input);
			while (input != "N" && input != "Y")
			{ // Check validity of input.

				std::cout << "Please enter 'Y' or 'N'" << std::endl;
				std::getline(std::cin, input);
			}
			if (input == "N")
			{ // No dependency requirement. Ask for user's confirmation.
				
				int duration;
				std::cout << "Add duration in format: integer. " << std::endl;
				std::cin >> duration;

				std::cout << "Confirm to add job?[Y/N]" << std::endl;
				std::getline(std::cin, input);
				while (input != "N" && input != "Y")
				{ // Check the validity of input.

					std::cout << "Please enter 'Y' or 'N'" << std::endl;
					std::getline(std::cin, input);
				}
				if (input == "Y")
				{ // Confirm the job.

					Job job(boost::posix_time::time_from_string(datetime), job_description, duration);
					all_jobs[boost::posix_time::time_from_string(datetime)] = job;
					_historicalLog.getHistotoricalSchedule(date).addJob(all_jobs[boost::posix_time::time_from_string(datetime)]);
					std::cout << "Job added." << std::endl;
					std::cout << "----------------------" << std::endl;
					std::cout << "Please give another command." << std::endl;
				}
				else
				{ // Abort the command. Do not create the job.
					std::cout << "Aborted." << std::endl;
					std::cout << "----------------------" << std::endl;
					std::cout << "Please give another command." << std::endl;
				}
			}
			else
			{ // Has dependency requirement. Ask for dependency according to time.

				std::cout << "Enter the job dependency in date and time format: 'YYYY-MM-DD HH:MM:SS'." << std::endl;
				std::getline(std::cin, input);
				while (checkDateTime(input) == 0)
				{ // Check the validity of date input.

					std::cout << "Invalid format. Please enter again: 'YYYY-MM-DD HH:MM:SS'." << std::endl;
					std::getline(std::cin, input);
				}
				auto dt = input;

				int duration;
				std::cout << "Add duration in format: integer. " << std::endl;
				std::cin >> duration;

				// Ask for user's confirmation.
				std::cout << "Confirm to add job?[Y/N]" << std::endl;
				std::getline(std::cin, input);
				while (input != "N" && input != "Y")
				{ // Check validity of input.

					std::cout << "Please enter 'Y' or 'N'" << std::endl;
					std::getline(std::cin, input);
				}
				if (input == "Y")
				{ // Confirm the job.

					Job job(boost::posix_time::time_from_string(datetime), job_description, all_jobs[boost::posix_time::time_from_string(dt)], duration, 0);
					all_jobs[boost::posix_time::time_from_string(datetime)] = job;
					_historicalLog.getHistotoricalSchedule(date).addJob(all_jobs[boost::posix_time::time_from_string(datetime)]);
					std::cout << "Job added." << std::endl;
					std::cout << "----------------------" << std::endl;
					std::cout << "Please give another command." << std::endl;
				}
				else
				{ // Abort the command. Do not create the job.
					std::cout << "Aborted.";
					std::cout << "----------------------" << std::endl;
					std::cout << "Please give another command." << std::endl;
				}
			}
		}
		else
		{ // Deal with the case when user's input does not match any command.

			std::cout << "Please enter a valid job. Enter 'help' for more instructions." << std::endl;
		}
	}
}


#endif // !USERINTERFACE_H







